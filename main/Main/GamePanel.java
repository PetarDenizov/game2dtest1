package Main;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import tile.tilemanager;




public class GamePanel extends JPanel implements Runnable {
	//Frame dimensions
	private final int originalTileSize = 16;
	private final int scale = 4;
	private final int tilesize =  originalTileSize * scale;
    private  final int maxScreenCol = 16;
    private final int maxScreenRow = 12;
    private final int ScreenWidth = getTilesize() * maxScreenCol; //768 pixels
    private final int ScreenHight = getTilesize() * maxScreenRow; //576 pixels
    //world settings
    private final int maxWorldCol = 30;
    private final int maxWorldRow = 30;
    private final int WorldWidth = getTilesize() * getMaxWorldCol();
    private final int WorldHight = getTilesize() *getMaxWorldRow();
    //hero position and speed
    //FPS
    int FPS = 60;
    Thread gameThread;
   tilemanager  tm = new tilemanager(this);
    KeyCommnads KC = new   KeyCommnads();
 public Hero hero = new Hero(KC,this);
    
    public GamePanel() {
    	this.setPreferredSize(new Dimension(ScreenWidth , ScreenHight));
    	this.setBackground(Color.black);
    	this.setDoubleBuffered(true);
    	this.addKeyListener(KC);
    	this.setFocusable(true);
    	
    	
    }
    public void StartGameThread() {
    	gameThread = new Thread(this);
    	gameThread.start();
    }
	@Override
	public void run() {
		double drawInterval = 1000000000/FPS;
		double delta = 0;
		long lastTime = System.nanoTime();
		long currentTime;
		while (gameThread != null) {
			currentTime = System.nanoTime();
			delta += (currentTime - lastTime) / drawInterval;
			lastTime = currentTime;
			if (delta>1) {
				//Update: update the information
				//Draw:  draw the screen with the updated information
				update();
				repaint();
				delta--;
			}
			
		}
		
	}
	public void update() {
		hero.update();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 =  (Graphics2D)g;
		tm.draw(g2);
		hero.paint(g2);
		g2.dispose();
	}
	
	
	
	
	public int getOriginalTileSize() {
		return originalTileSize;
	}
	public int getScale() {
		return scale;
	}
	public int getTilesize() {
		return tilesize;
	}
	public int getMaxScreenCol() {
		return maxScreenCol;
	}
	public int getMaxScreenRow() {
		return maxScreenRow;
	}
	public int getScreenWidth() {
		return ScreenWidth;
	}
	public int getScreenHight() {
		return ScreenHight;
	}
	public int getMaxWorldCol() {
		return maxWorldCol;
	}
	public int getMaxWorldRow() {
		return maxWorldRow;
	}
	public int getWorldWidth() {
		return WorldWidth;
	}
	public int getWorldHight() {
		return WorldHight;
	}
	
	
	}

