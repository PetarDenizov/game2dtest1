package Main;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Hero extends Entity {
	GamePanel Gp;
	KeyCommnads KC;
	private int worldx ;
	private int worldy ;
	private int screenx;
	private int screeny;
	private int playerspeed=4;
	private String direction = "down";
	public int getWorldx() {
		return worldx;
	}

	public void setWorldx(int worldx) {
		this.worldx = worldx;
	}

	public int getWorldy() {
		return worldy;
	}

	public void setWorldy(int worldy) {
		this.worldy = worldy;
	}

	public int getPlayerspeed() {
		return playerspeed;
	}

	public void setPlayerspeed(int playerspeed) {
		this.playerspeed = playerspeed;
	}
	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}
	public int getScreenx() {
		return screenx;
	}

	public void setScreenx(int screenx) {
		this.screenx = screenx;
	}

	public int getScreeny() {
		return screeny;
	}

	public void setScreeny(int screeny) {
		this.screeny = screeny;
	}
	
public Hero(KeyCommnads KC,GamePanel Gp) {
	this.Gp = Gp;
	this.KC = KC;
	this.screenx = Gp.getScreenWidth()/2-(Gp.getTilesize()/2);
	this.screeny = Gp.getScreenHight()/2-(Gp.getTilesize()/2);
	this.worldx = Gp.getTilesize() * 20;
	this.worldy = Gp.getTilesize() * 10;
	getHeroImage();
}
public void getHeroImage() {
	try{
		setUp1(ImageIO.read(getClass().getResourceAsStream("/hero/up1.png")));
		setUp2(ImageIO.read(getClass().getResourceAsStream("/hero/up2.png")));
		setDown1(ImageIO.read(getClass().getResourceAsStream("/hero/down1.png")));
		setDown2(ImageIO.read(getClass().getResourceAsStream("/hero/down2.png")));
		setLeft1(ImageIO.read(getClass().getResourceAsStream("/hero/left1.png")));
		setLeft2(ImageIO.read(getClass().getResourceAsStream("/hero/left2.png")));
		setRight1(ImageIO.read(getClass().getResourceAsStream("/hero/right1.png")));
		setRight2(ImageIO.read(getClass().getResourceAsStream("/hero/right2.png")));
	}catch(IOException e){
		e.printStackTrace();
	}
}

public void update() {
	if(KC.isUpPressed()==true || KC.isDownPressed()==true || KC.isLeftPressed()==true || KC.isRightPressed()==true) {
		if(KC.isUpPressed()==true) {
			setDirection( "up" );
			setWorldy(getWorldy() - getPlayerspeed());
		}
		else if(KC.isDownPressed()==true) {
			setDirection( "down" );
			setWorldy(getWorldy()  + getPlayerspeed());
	      }
		else if(KC.isLeftPressed()==true) {
			setDirection( "left" );
			setWorldx(getWorldx() - getPlayerspeed());
		}
		else if(KC.isRightPressed()==true) {
			setDirection( "right" );
			setWorldx(getWorldx() + getPlayerspeed());
		}
		setSpriteCounter(getSpriteCounter() + 1);
		if(getSpriteCounter()>12) {
			if(getSpriteNum() == 1) {
				setSpriteNum(2);
			}
			else if(getSpriteNum() == 2) {
				setSpriteNum(1);
			}
			setSpriteCounter(0);
		
	}
	}
	
}
public void paint(Graphics2D g2) {
	//g2.setColor(Color.white);
	//g2.fillRect(getPosx(),getPosy(), Gp.getTilesize(),Gp.getTilesize());
	BufferedImage  image = null;
	switch(getDirection()) {
	case "up" :
		if(getSpriteNum() == 1) {
			image = getUp1();
		}
		if(getSpriteNum() == 2) {
			image = getUp2();
		}
		break;
	case "down" :
		if(getSpriteNum() == 1) {
			image = getDown1();
		}
		if(getSpriteNum() == 2) {
			image = getDown2();
		}
		break;
	case "left" :if(getSpriteNum() == 1) {
		image = getLeft1();
	}
	if(getSpriteNum() == 2) {
		image = getLeft2();
	}
	break;
	case "right" :
		if(getSpriteNum() == 1) {
		image = getRight1();
	}
	if(getSpriteNum() == 2) {
		image = getRight2();
	} break;
	
	}
	g2.drawImage(image, this.screenx,this.screeny, Gp.getTilesize(),Gp.getTilesize(),null);
}


}


