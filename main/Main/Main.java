package Main;
import javax.swing.JFrame;

public class Main {
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
     JFrame window = new JFrame();
      GamePanel gamepanel = new GamePanel( );
     window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     window.setResizable(false);
     window.setTitle("FirstAdventure");
     window.add(gamepanel);
     window.pack();
     window.setLocationRelativeTo(null);
     window.setVisible(true);
     gamepanel.StartGameThread();
     
	}

}
