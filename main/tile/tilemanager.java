package tile;

import java.awt.Graphics2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.imageio.ImageIO;

import Main.GamePanel;

public class tilemanager {
GamePanel gp;
Tile[] tile;
int mapTileNum[][];

public tilemanager(GamePanel gp) {
	this.gp = gp;
	tile = new Tile[10];
	mapTileNum =  new int[gp.getMaxWorldCol()][gp.getMaxWorldRow()];
	loadMap("/maps/map01.txt");
	getTileImage();
	

	
	
}

public void getTileImage() {
	try {
		tile[0] = new Tile();
		tile[0].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/sand.png"));
		
		tile[1] = new Tile();
		tile[1].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/water.png"));
		
		tile[2] = new Tile();
		tile[2].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/tree.png"));
		
		tile[3] = new Tile();
		tile[3].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/dirt.png"));
		
		tile[4] = new Tile();
		tile[4].Image = ImageIO.read(getClass().getResourceAsStream("/tiles/sand.png"));
		
	}catch(IOException e) {
		e.printStackTrace();
	}
}
public void loadMap(String s) {
	try {
		InputStream is = getClass().getResourceAsStream(s);
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		int col = 0;
		int row = 0;
		while(col < gp.getMaxWorldCol() && row < gp.getMaxWorldRow()) {
			String line = br.readLine();
		
		
			while(col < gp.getMaxWorldCol() ) {
				String number[] = line.split(" ");
				int num = Integer.parseInt(number[col]);
				mapTileNum[col][row] = num;
				col++;
			}
				if(col == gp.getMaxWorldCol()) {
					col = 0;
					row ++;
				}
			
			}
			
		br.close();
		
	}catch(Exception e){
		
	}
}


public void draw(Graphics2D g2) {
	int  Worldcol = 0;
	int  Worldrow = 0;
	while(Worldcol < gp.getMaxWorldCol() && Worldrow < gp.getMaxWorldRow() ){
		int tileNum =  mapTileNum[Worldcol][Worldrow];
		int WorldX = Worldcol * gp.getTilesize();
		int WorldY = Worldrow * gp.getTilesize();
		int screenx = WorldX - gp.hero.getWorldx() + gp.hero.getScreenx();
		int screeny = WorldY - gp.hero.getWorldy() + gp.hero.getScreeny();
		if(WorldX + gp.getTilesize() > gp.hero.getWorldx() - gp.hero.getScreenx() && WorldX - gp.getTilesize() < gp.hero.getWorldx() + gp.hero.getScreenx()
		&&WorldY  + gp.getTilesize()> gp.hero.getWorldy() - gp.hero.getScreeny() && WorldY - gp.getTilesize() < gp.hero.getWorldy() + gp.hero.getScreeny()){
			g2.drawImage(tile[tileNum].Image , screenx ,screeny,gp.getTilesize(),gp.getTilesize(),null);
		}
		
		Worldcol++;
	
		if(Worldcol == gp.getMaxWorldCol()) {
			Worldcol = 0;
			Worldrow++;
			
		}
		
	}
	
}
}